# Course project

The idea of the project is to look into one of the three aspects of mobile robotics we covered in the class:

 - **Perception** or Computer Vision
 - **Learning** such as reinforcement learning
 - **Planning** such as Belief Space Planning, POMDPs and SLAM
 
Another aspect of this course is to be able to apply the theory in some practical application. In order to achieve this, we suggest to use two of these avenues:

 - Using a **middleware** such as ROS
 - Using **simulation** such as Gazebo (and Turtlebot or Pioneer 2)
 - Using GPU-based deep learning framework **Tensorflow** (including colaboratory)

> The project should utilize the knowledge developed in the class and also represent a significant effort on self-exploration


## Evaluation criteria

| Criteria | Weightage |
| ------ | ------ |
| Application of the theory to get a working  prototype | 0.60 |
| Understanding of the field at abstract level | 0.10 |
| Understanding the chosen topic in some depth | 0.15 |
| Presentation skills and clarity | 0.05 |
| Answering other questions & participation | 0.10 |
| Late submission | -0.20 |




# Chosen topics

 **Tom** and **Timon**: _Pedestrian detection in a frame and tracking over frames_    
 **Marc-Peter** and **Philipp**: _Using SLAM to navigate robots in uncertain environments (Simulation with Gazebo)_    
 **Daniel** and **Joachim**: _Traffic light detection and decision if braking is needed/possible_
    
# Deadline

The project needs to be completed by 23:59 CEST, **30th of April, 2018**. Late submission deadline is a week later, **7th of May, 2018**. Presentation would be held at time suitable for the whole class.


----
shashank.pathak@visteon.com
Visteon Electronics GmbH

[//]: # (Some rants here. Not for the people outside!)

