# Simulated mobile robotics

This assumes following knowledge:
 - overview of planning
 - overview of learning
 - overview of perception (not just vision)
 - overview of middleware (specifically ROS)

We have covered each of these. If required, look back into the slides or even discuss with your classmates or the instructor (me).

# Using turtlebot simulator

Turtlebots are very cost-effective platform for doing serious robotics research at the starting level. They are sufficient to demonstrate all four pillars of this course - planning, learning, perception and the middleware.

## Getting started

We need to install the apps, so do this:
```sh
$ sudo apt-get install ros-kinetic-turtlebot-apps ros-kinetic-turtlebot-rviz-launchers
```

Get in your kinetic workspace and bringup the Turtlebot as:
```sh
roslaunch turtlebot_gazebo turtlebot_world.launch
```

In another tab or terminal, we will start the teleop package as:
```sh
roslaunch turtlebot_teleop keyboard_teleop.launch
```

To visualize graphically, we use Rviz. There are relevant package pre-made for us so we use it simply as:
```sh
roslaunch turtlebot_rviz_launchers view_robot.launch
```

In order to do something non-trivial we include some geometrical objects in the world. This is also done for tutorial examples:
```sh
$ export TURTLEBOT_BASE=create
$ export TURTLEBOT_STACKS=circles
$ export TURTLEBOT_3D_SENSOR=asus_xtion_pro
$ roslaunch turtlebot_gazebo turtlebot_playground.launch
```

In order to start mapping this surrounding, we call another launch file, i.e.,:
```sh
roslaunch turtlebot_gazebo gmapping_demo.launch
```

In order to visualize the map which is generated at each instance, we can use rviz:
```sh
roslaunch turtlebot_rviz_launchers view_navigation.launch
```

Finally, we save our map as:
```sh
rosrun map_server map_saver -f <your map name>
```

Another example:
```sh
roslaunch turtlebot_gazebo amcl_demo.launch
```

# Using commandline arguments

We can access various variables exposed via commandline, while launching a Turtlebot process. For example to change the name of the robot:
```sh
$ roslaunch turtlebot_bringup minimal.launch robot_name:=XYZ --screen
```

The argument for seeing other argument is not --help but:
```sh
--ros-args
```

# Using environment variables

Instead of specifying the variables in the commandline, we can change the environment variables which are set for this purpose. They are TURTLEBOT_*. Use grep to catch them all and modify that you need to. E.g.,
```sh
env | grep TURTLEBOT*NAME
```

# Using launch file itself

Launch file is an easier way to run repeated sets of ros commands. Hence we can specify our arguments even there.
```sh
<launch>
  <include file="$(find turtlebot_bringup)/launch/minimal.launch">
    <arg name="robot_name" value="DHBW"/>
    <arg name="robot_icon" value="rocon_icons/cybernetic_pirate.png"/>
  </include>
</launch>
```

# More of launch file
Some of the more used aspects of the XML launch file in ros are:

```sh
$(optenv ENVIRONMENT_VARIABLE default_value)
```

```sh
<param name="foo" value="$(optenv VARIABLE bar)" />
```

```sh
$(find pkg)
```

```sh
$(anon node_name)
```
E.g.,
```sh
<node name="$(anon foo)" pkg="rospy_tutorials" type="talker.py" />
```

```sh
$(arg foo)
```
E.g.,
```sh
<param name="foo" value="$(arg my_foo)" />
```
so that the commandline calling could be:
```sh
roslaunch some_pkg some_file.launch my_foo:=10
```

Full-ish example:
```sh
<launch>
  <!-- local machine already has a definition by default.
       This tag overrides the default definition with
       specific ROS_ROOT and ROS_PACKAGE_PATH values -->
  <machine name="local_alt" address="localhost" default="true" ros-root="/u/user/ros/ros/" ros-package-path="/u/user/ros/ros-pkg" />
  <!-- a basic listener node -->
  <node name="listener-1" pkg="rospy_tutorials" type="listener" />
  <!-- pass args to the listener node -->
  <node name="listener-2" pkg="rospy_tutorials" type="listener" args="-foo arg2" />
  <!-- a respawn-able listener node -->
  <node name="listener-3" pkg="rospy_tutorials" type="listener" respawn="true" />
  <!-- start listener node in the 'wg1' namespace -->
  <node ns="wg1" name="listener-wg1" pkg="rospy_tutorials" type="listener" respawn="true" />
  <!-- start a group of nodes in the 'wg2' namespace -->
  <group ns="wg2">
    <!-- remap applies to all future statements in this scope. -->
    <remap from="chatter" to="hello"/>
    <node pkg="rospy_tutorials" type="listener" name="listener" args="--test" respawn="true" />
    <node pkg="rospy_tutorials" type="talker" name="talker">
      <!-- set a private parameter for the node -->
      <param name="talker_1_param" value="a value" />
      <!-- nodes can have their own remap args -->
      <remap from="chatter" to="hello-1"/>
      <!-- you can set environment variables for a node -->
      <env name="ENV_EXAMPLE" value="some value" />
    </node>
  </group>
</launch>
```


# Using SLAM

Turtlebot navigation is the package to do default SLAM with the Turtlebot. It can be divided into: motion, localization and map building itself.

## Moving
In order to move, it uses the package move_base.

## Localization
Here the package is amcl.
An example to localize using a scan topic say my_scan would be:
```sh
amcl scan:=my_scan
```
It works on laser scans and use these algos: sample_motion_model_odometry, beam_range_finder_model, likelihood_field_range_finder_model, Augmented_MCL, and KLD_Sampling_MCL.


## Map building
This is done by the package gmapping.


    
    


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [dill]: <https://github.com/joemccann/dillinger>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [john gruber]: <http://daringfireball.net>
   [df1]: <http://daringfireball.net/projects/markdown/>
   [markdown-it]: <https://github.com/markdown-it/markdown-it>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
   [PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>
   [PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>

